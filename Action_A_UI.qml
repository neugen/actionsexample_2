import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

import Test 1.0

Frame {
    visible: presenter.state !== Request_A_Presenter.State_None

    Request_A_Presenter {
        id: presenter
    }

    StackLayout {
        currentIndex: {
            if (presenter.state == Request_A_Presenter.State_Value) {
                return 1
            }
            else if (presenter.state == Request_A_Presenter.State_Approve) {
                return 2
            }

            return 0
        }

        Item {}

        Column {
            Frame {
                TextEdit {
                    id: edit
                    text: presenter.value
                }
            }

            Button {
                text: "accept"
                onClicked: {
                    presenter.apply(edit.text)
                }
            }
        }

        Column {
            spacing: 10

            Label {
                text: "approve is needed"
            }

            RowLayout {
                Button {
                    text: "ok"
                    onClicked: {
                        presenter.approve(true)
                    }
                }

                Button {
                    text: "cancel"
                    onClicked: {
                        presenter.approve(false)
                    }
                }
            }
        }
    }
}
