# ActionsExample_2

# Simple example of mechanism for implement request-reply behaviour

>>>
Very often we need to fulfill a request and get a response.

The problems here lie in the fact that you can violate the principles of SOLID and, as a result, break down the architecture of the application.

>>>

### Let's try to untie the layers using the proposed mechanism.


First of all, you need to register a new **RequestImpl** subclass by describing its name, return value and input parameters:
```C++
REGISTER_REQUEST(SomeRequest, QString, int, bool)
```


To access the request objects, a **RequestsRegistry** is used.
Here we need to attach previously registered requests:
```C++
RequestsRegistry registry;
registry.attach<SomeRequest>();
```


After request was registered and attached to corresponding **RequestsRegistry** we can get it and use for retrieving data:
```C++
if (SomeRequest* impl = registry.get<SomeRequest>())
{
    impl->withArgs(1, true).getResult([this](QString result)
    {
        // use result here
    });
}
else
{
    // request delegate was not created
}
```
or 
```C++
registry.get<SomeRequest>(1, true).getResult([this](QString result)
{
    // use result here
}
```



How do requests gets to the **RequestsRegistry**?

To communicate with request object we needed to create a **RequestImpl::Delegate**. It is possible to create two types of delegates: *normal* and *interruptible*.

*Interruptible* means that if somebody tries to perform any request current (*active*) interruptible request will be *deactivated*.
```C++
SomeRequest::Delegate request = SomeRequest::make() // or makeInterruptible();

request.onActivated([this](int iParam, bool bParam)
{
    // somebody starts request and pass some arguments

}).onDeactivated([this]()
{
    // request was finished by calling request.setResponse() or interrupted
});

bool active = request.isActive(); // true
request.setResponse("abcdefg");
active = request.isActive(); // false
```

>>>
Under the hood to communicate with request and response sides **QThread**, **std::promise** and **std::future** are used.
>>>

