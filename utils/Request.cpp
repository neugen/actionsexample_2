#include "Request.h"

#include <future>
#include <cassert>
#include <exception>

#include "RequestThread.h"

namespace
{
static Request::Id s_lastId = 0;
static Request* s_currentInterruptible = nullptr;

} // anonymous namespace

struct RequestData
{
    bool active = false;
    bool interruptible = false;
    QObject threadContext;
    std::promise<QVariant> promise;
    std::function<void()>* deactivationCallback = nullptr;

    explicit RequestData() = default;

    explicit RequestData(bool interruptible)
        : interruptible(interruptible)
    {}
};

Request::Id Request::makeId()
{
    return ++s_lastId;
}

Request::Request(bool interruptible)
    : p(std::make_unique<RequestData>(interruptible))
{
}

Request::~Request()
{
    interrupt();
}

bool Request::isActive() const
{
    return p->active;
}

void Request::cancelRequest()
{
    interrupt();
}

void Request::reply(QVariant value)
{
    if (!p->active)
        return;

    p->promise.set_value(value);
}

void Request::init(std::function<void()>* deactivationCallback)
{
    p->deactivationCallback = deactivationCallback;
}


void Request::perform(std::function<void(QVariant)>&& callback)
{
    beforePerform();

    p->promise = {};

    RequestThread* thread = new RequestThread([this]()
    {
        auto future = p->promise.get_future();

        try
        {
            const QVariant value = future.get();
            return RequestThreadResult(value);
        }
        catch(...) // interruption
        {
            return RequestThreadResult();
        }
    });

    QObject::connect(thread, &RequestThread::finished, thread, &QObject::deleteLater);
    QObject::connect(thread, &RequestThread::resultReady, &p->threadContext,
                     [this, callback=std::move(callback)](RequestThreadResult result)
    {
        p->active = false;

        if (p->interruptible && result.interrupted)
        {
            assert(*p->deactivationCallback
                   && "deactivation callback was not set for interruptible request");
        }

        if (*p->deactivationCallback)
            (*p->deactivationCallback)();

        if (!result.interrupted)
            callback(result.value);
    });

    p->active = true;
    thread->start();
}

void Request::interrupt()
{
    if (!p->interruptible || !p->active)
        return;

    if (this == s_currentInterruptible)
        s_currentInterruptible = nullptr;

    try
    {
        throw std::runtime_error("request interruption");
    }
    catch(...)
    {
        try
        {
            p->promise.set_exception(std::current_exception());
        }
        catch(...) { /* set_exception() may throw */ }
    }
}

void Request::beforePerform()
{
    if (s_currentInterruptible)
        s_currentInterruptible->interrupt();

    if (p->interruptible)
        s_currentInterruptible = this;
}
