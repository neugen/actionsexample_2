#pragma once

#include <functional>

#include <QThread>
#include <QVariant>
#include <QMetaType>

struct RequestThreadResult
{
    QVariant value;
    bool interrupted = false;

    explicit RequestThreadResult() = default;

    explicit RequestThreadResult(QVariant value)
        : value(value)
    {}
};

class RequestThread : public QThread
{
    Q_OBJECT

    std::function<RequestThreadResult()> c_routine;

public:
    explicit RequestThread(std::function<RequestThreadResult()>&& routine)
        : c_routine(std::move(routine))
    {
        qRegisterMetaType<RequestThreadResult>("RequestThreadResult");
    }

private: // QThread
    void run() override { emit resultReady(c_routine()); }

signals:
    void resultReady(RequestThreadResult value);

};
