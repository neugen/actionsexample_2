#pragma once

#include <memory>

#include "RequestBackendImpl.h"

struct RequestData;

class Request : protected RequestBackendImpl
{
    std::unique_ptr<RequestData> p;

protected:
    explicit Request(bool interruptible = false);
    ~Request();

public:
    using Id = int;

    virtual Id id() const = 0;
    virtual void detach() = 0;

    void perform(std::function<void(QVariant)>&& callback);
    void interrupt();

protected:
    static Id makeId();

private: // RequestBackendImpl
    bool isActive() const override;
    void cancelRequest() override;
    void reply(QVariant value) override;
    void init(std::function<void()>* deactivationCallback) override;

private:
    void beforePerform();

};
