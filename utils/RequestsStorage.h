#pragma once

class Request;

class RequestsStorage
{
protected:
    ~RequestsStorage() = default;

public:
    virtual void add(Request* request) = 0;
    virtual void remove(Request* request) = 0;

};
