#pragma once

#include "RequestBackendImpl.h"
#include "traits.h"

template <typename Signature>
class RequestBackend
{
    using traits = function_traits<Signature>;

public:
    using Result = typename traits::return_type;
    using ArgsCallback = typename traits::args_callback;

    struct Callbacks
    {
        ArgsCallback activationCallback;
        std::function<void()> deactivationCallback;
    };

public:
    RequestBackend(RequestBackendImpl* impl)
        : m_impl(impl)
    {
        m_impl->init(&m_callbacks.deactivationCallback);
    }

    virtual ~RequestBackend() = default;

    RequestBackend(RequestBackend&&) = default;
    RequestBackend& operator=(RequestBackend&&) = default;

    Callbacks& callbacks() { return m_callbacks; }

    bool isActive() const { return m_impl->isActive(); }

    void cancelRequest() { m_impl->cancelRequest(); }

    void reply(const Result& value)
    {
        m_impl->reply(QVariant::fromValue(value));
    }

private:
    Callbacks m_callbacks;
    RequestBackendImpl* m_impl = nullptr;

};
