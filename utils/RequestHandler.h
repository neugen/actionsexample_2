#pragma once

#include <memory>

#include "RequestBackend.h"

struct RequestDelegateData;

template <typename Signature>
class RequestHandler
{
public:
    using Backend = RequestBackend<Signature>;

    using Result = typename Backend::Result;
    using Callback = typename Backend::ArgsCallback;

    explicit RequestHandler(std::unique_ptr<Backend>&& backend)
        : m_backend(std::move(backend))
    {}

    ~RequestHandler();

    RequestHandler(RequestHandler&&) = default;
    RequestHandler& operator=(RequestHandler&&) = default;

    RequestHandler& onActivated(Callback&& callback)
    {
        m_backend->callbacks().activationCallback = std::move(callback);
        return *const_cast<RequestHandler*>(this);
    }

    void onDeactivated(std::function<void()>&& callback)
    {
        m_backend->callbacks().deactivationCallback = std::move(callback);
    }

    bool isActive() const { return m_backend->isActive(); }
    void reply(const Result& value) { m_backend->reply(value); }

private:
    std::unique_ptr<Backend> m_backend;

};


template <typename Signature>
RequestHandler<Signature>::~RequestHandler() = default;
