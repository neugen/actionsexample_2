#pragma once

#include "Request.h"
#include "RequestHandler.h"
#include "RequestsStorage.h"

struct none_t {};
Q_DECLARE_METATYPE(none_t)

template <typename Name, typename Signature>
class RequestImpl
        : protected Request
        , protected RequestBackend<Signature>
{
    friend class RequestsRegistry;

    static const Id ID;
    static RequestsStorage* s_storage;

    using Backend = RequestBackend<Signature>;
    using Result = typename Backend::Result;

    using ResultCallback = std::function<void(const Result&)>;
    using PerformCallback = std::function<void(ResultCallback&&)>;

public:
    explicit RequestImpl(bool interruptible = false)
        : Request(interruptible)
        , Backend(this)
    {
        assert(s_storage && "interface was not attached!");

        if (s_storage)
            s_storage->add(this);
    }

    ~RequestImpl()
    {
        if (s_storage)
            s_storage->remove(this);
    }

    using Handler = RequestHandler<Signature>;

    static Handler make()
    {
        return Handler(std::unique_ptr<Backend>(new RequestImpl()));
    }

    static Handler makeInterruptible()
    {
        return Handler(std::unique_ptr<Backend>(new RequestImpl(true)));
    }

    class ProcessWrapper
    {
        Request* m_request = nullptr;
        std::function<void()> m_activationCallback;

    public:
        explicit ProcessWrapper() = default;

        explicit ProcessWrapper(Request* request,
                                std::function<void()>&& activationCallback)
            : m_request(request)
            , m_activationCallback(std::move(activationCallback))
        {}

        void process(ResultCallback&& callback) const
        {
            if (m_activationCallback)
            {
                m_request->perform([callback=std::move(callback)](QVariant result)
                {
                    callback(result.value<Result>());
                });

                m_activationCallback();
            }
        }

    };

    template <typename... Args>
    ProcessWrapper withArgs(Args&&... args)
    {
        if (Backend::isActive() || !Backend::callbacks().activationCallback)
            return ProcessWrapper();

        return ProcessWrapper(this, [this, args...]()
        {
            Backend::callbacks().activationCallback(args...);
        });
    }

    void process(ResultCallback&& callback)
    {
        return ProcessWrapper(this, [this]()
        {
            callbacks().activationCallback();

        }).process(std::move(callback));
    }

private: // ActionRequest
    Id id() const override { return ID; }
    void detach() override { s_storage = nullptr; }

};


template <typename Name, typename Signature>
const Request::Id RequestImpl<Name, Signature>::ID = Request::makeId();

template <typename Name, typename Signature>
RequestsStorage* RequestImpl<Name, Signature>::s_storage = nullptr;


#define REGISTER_REQUEST(Name, Signature) \
    class Name : public RequestImpl<Name, Signature> {};
