#pragma once

#include <functional>

template<class F>
struct function_traits;

template<class R, class... Args>
struct function_traits<R(*)(Args...)>
    : public function_traits<R(Args...)>
{};

template<class R, class... Args>
struct function_traits<R(Args...)>
{
    using return_type = R;
    using args_callback = std::function<void(Args...)>;
};

template<class F>
struct function_traits
{
private:
    using call_type = function_traits<decltype(&F::type::operator())>;

public:
    using Result = typename call_type::return_type;
    using ArgsCallback = typename call_type::args_callback;
};
