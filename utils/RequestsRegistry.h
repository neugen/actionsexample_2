#pragma once

#include <map>
#include <cassert>

#include "Request.h"
#include "RequestsStorage.h"

class RequestsRegistry : protected RequestsStorage
{
    std::map<Request::Id, Request*> m_data;

public:
    ~RequestsRegistry()
    {
        for (auto item : m_data)
        {
            item.second->detach();
        }
    }

    template <typename TRequestImpl>
    void attach()
    {
        assert(!TRequestImpl::s_storage && "interface is already attached!");
        TRequestImpl::s_storage = static_cast<RequestsStorage*>(this);
    }

    template <typename TRequestImpl>
    TRequestImpl* get()
    {
        auto it = m_data.find(TRequestImpl::ID);

        if (it != m_data.end())
            return static_cast<TRequestImpl*>(it->second);

        return nullptr;
    }

    template <typename TRequestImpl, typename... Args>
    typename TRequestImpl::ProcessWrapper get(Args&&... args)
    {
        if (auto* impl = get<TRequestImpl>())
            return impl->withArgs(std::forward<Args>(args)...);

        return TRequestImpl::ProcessWrapper();
    }

private: // RequestsStorage
    void add(Request* request) override
    {
        m_data.insert(std::make_pair(request->id(), request));
    }

    void remove(Request* request) override
    {
        auto it = m_data.find(request->id());

        if (it != m_data.end())
            m_data.erase(it);
    }

};
