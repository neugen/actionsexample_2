#pragma once

#include <functional>

#include <QVariant>

class RequestBackendImpl
{
protected:
    RequestBackendImpl() = default;
    ~RequestBackendImpl() = default;

public:
    virtual bool isActive() const = 0;
    virtual void cancelRequest() = 0;
    virtual void reply(QVariant value) = 0;

    virtual void init(std::function<void()>* deactivationCallback) = 0;

};
