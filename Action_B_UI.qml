import QtQuick 2.12
import QtQuick.Controls 2.12

import Test 1.0

Frame {
    visible: presenter.active

    Request_B_Presenter {
        id: presenter
    }

    Column {
        Label {
            text: slider.value
        }

        Slider {
            id: slider

            from: 0
            to: 100

            value: presenter.value
        }

        Button {
            text: "accept"
            onClicked: {
                presenter.setValue(slider.value)
            }
        }
    }
}
