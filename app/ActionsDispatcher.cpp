#include "ActionsDispatcher.h"

#include "utils/RequestsRegistry.h"
#include "Requests.h"

struct ActionsDispatcherData
{
    Payload* payload = nullptr;
    RequestsRegistry registry;

    explicit ActionsDispatcherData()
    {
        registry.attach<RetrieveIntValue>();
        registry.attach<ApproveIntValue>();

        registry.attach<RetrieveDoubleValue>();

        registry.attach<RetrieveSomeAction>();
    }
};

ActionsDispatcher::ActionsDispatcher(QObject* parent)
    : QObject(parent)
    , p(std::make_unique<ActionsDispatcherData>())
{
}

ActionsDispatcher::~ActionsDispatcher()
{
}

void ActionsDispatcher::setPayload(Payload* value)
{
    p->payload = value;
}

void ActionsDispatcher::doAction_A()
{
    p->registry
            .get<RetrieveIntValue>(int(p->payload->value()))
            .process([this](int value)
    {
        if (value >= 0 && value <= 100)
        {
            p->payload->setValue(value);
            return;
        }

        p->registry
                .get<ApproveIntValue>("Value is outside bounds of [0..100]")
                .process([this, value](bool approved)
        {
            if (approved)
                p->payload->setValue(value);
        });
    });
}

void ActionsDispatcher::doAction_B()
{
    if (auto* impl = p->registry.get<RetrieveDoubleValue>())
    {
        impl->withArgs(p->payload->value()).process([this](double value)
        {
            p->payload->setValue(value);
        });
    }
    else
    {
        p->payload->setValue(9876.54321);
    }
}

void ActionsDispatcher::doAction_C()
{
    if (auto* impl = p->registry.get<RetrieveSomeAction>())
    {
        impl->process([this](auto)
        {
            const double value = p->payload->value();

            if (value > 50)
            {
                p->payload->setValue(100);
            }
            else
            {
                p->payload->setValue(0);
            }
        });
    }
    else
    {
        p->payload->setValue(50);
    }
}
