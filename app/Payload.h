#pragma once

#include <QObject>

class IPayload
{
public:
    virtual double value() const = 0;
};

class Payload
        : public QObject
        , public IPayload
{
    Q_OBJECT

    double m_value = 0;

    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    explicit Payload(QObject* parent = nullptr);

    double value() const override { return m_value; }
    void setValue(double value);

signals:
    void valueChanged();
};

