#include "System.h"

#include "Payload.h"
#include "ActionsDispatcher.h"

struct SystemData
{
    Payload payload;
    ActionsDispatcher dispatcher;

    explicit SystemData()
    {
        dispatcher.setPayload(&payload);
    }
};

System::System(QObject* parent)
    : QObject(parent)
    , p(std::make_unique<SystemData>())
{
}

System::~System()
{
}

Payload* System::payload() const
{
    return &p->payload;
}

ActionsDispatcher* System::dispatcher() const
{
    return &p->dispatcher;
}
