#pragma once

#include <QString>

#include "utils/RequestImpl.h"

REGISTER_REQUEST(RetrieveIntValue, int(int))
REGISTER_REQUEST(ApproveIntValue, bool(QString))

REGISTER_REQUEST(RetrieveDoubleValue, double(double))

REGISTER_REQUEST(RetrieveSomeAction, none_t())
