#pragma once

#include <memory>

#include <QObject>

#include "Payload.h"

class ActionsDispatcher : public QObject
{
    Q_OBJECT

    std::unique_ptr<struct ActionsDispatcherData> p;

public:
    explicit ActionsDispatcher(QObject* parent = nullptr);
    ~ActionsDispatcher();

    void setPayload(Payload* value);

    Q_INVOKABLE void doAction_A();
    Q_INVOKABLE void doAction_B();
    Q_INVOKABLE void doAction_C();

};
