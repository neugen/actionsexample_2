import QtQuick 2.12
import QtQuick.Controls 2.12

import Test 1.0

Frame {
    visible: presenter.active

    Request_C_Presenter {
        id: presenter
    }

    Button {
        text: "just do it"
        onClicked: presenter.justDoIt();
    }
}
