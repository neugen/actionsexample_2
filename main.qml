import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

import Test 1.0

Window {
    width: 360
    height: 240

    visible: true

    title: qsTr("Hello World")

    Column {
        spacing: 10

        Label {
            text: "Current value is " + System.payload.value
        }

        RowLayout
        {
            Button {
                text: "A"
                onClicked: System.dispatcher.doAction_A()
            }

            Button {
                text: "B"
                onClicked: System.dispatcher.doAction_B()
            }

            Button {
                text: "C"
                onClicked: System.dispatcher.doAction_C()
            }
        }

        Row {
            spacing: 10

            Action_A_UI {}
            Action_B_UI {}
            Action_C_UI {}
        }
    }
}
