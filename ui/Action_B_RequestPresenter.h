#pragma once

#include <QObject>

#include "app/Requests.h"

class Action_B_RequestPresenter : public QObject
{
    Q_OBJECT

    double m_value = 0;
    bool m_active = false;
    RetrieveDoubleValue::Handler m_handler;

    Q_PROPERTY(bool active READ active NOTIFY activeChanged)
    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    explicit Action_B_RequestPresenter(QObject* parent = nullptr);

    bool active() const { return m_active; }
    void setActive(bool value);

    double value() const { return m_value; }
    Q_INVOKABLE void setValue(double value);

signals:
    void activeChanged();
    void valueChanged();
};

