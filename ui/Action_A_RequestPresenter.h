#pragma once

#include <QObject>
#include <QVariant>

#include "app/Requests.h"

class Action_A_RequestPresenter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int state READ state NOTIFY stateChanged)
    Q_PROPERTY(int value READ value NOTIFY valueChanged)
    Q_PROPERTY(QString info READ info NOTIFY infoChanged)

public:
    enum State
    {
        State_None,
        State_Value,
        State_Approve
    };
    Q_ENUM(State)

    explicit Action_A_RequestPresenter(QObject* parent = nullptr);

    int value() const { return m_value; }

    State state() const { return m_state; }
    void setState(State value);

    QString info() const { return m_info; }

    Q_INVOKABLE void apply(QVariant value);
    Q_INVOKABLE void approve(bool value);

private:
    QString m_info;
    int m_value = 0;

    State m_state = State_None;

    RetrieveIntValue::Handler m_valueHandler;
    ApproveIntValue::Handler m_approvalHandler;

signals:
    void stateChanged();
    void valueChanged();
    void infoChanged();

};

