#include "Action_A_RequestPresenter.h"

Action_A_RequestPresenter::Action_A_RequestPresenter(QObject* parent)
    : QObject(parent)
    , m_valueHandler(RetrieveIntValue::makeInterruptible())
    , m_approvalHandler(ApproveIntValue::makeInterruptible())
{
    m_valueHandler.onActivated([this](int value)
    {
        if (m_value != value)
        {
            m_value = value;
            emit valueChanged();
        }

        m_info.clear();
        emit infoChanged();

        setState(State_Value);

    }).onDeactivated([this]()
    {
        setState(State_None);
    });

    m_approvalHandler.onActivated([this](QString info)
    {
        m_info = info;
        emit infoChanged();

        setState(State_Approve);

    }).onDeactivated([this]()
    {
        setState(State_None);
    });
}

void Action_A_RequestPresenter::setState(State value)
{
    if (m_state != value)
    {
        m_state = value;
        emit stateChanged();
    }
}

void Action_A_RequestPresenter::apply(QVariant value)
{
    m_valueHandler.reply(value.toInt());
}

void Action_A_RequestPresenter::approve(bool value)
{
    m_approvalHandler.reply(value);
}
