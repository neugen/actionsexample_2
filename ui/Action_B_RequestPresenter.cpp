#include "Action_B_RequestPresenter.h"

Action_B_RequestPresenter::Action_B_RequestPresenter(QObject* parent)
    : QObject(parent)
    , m_handler(RetrieveDoubleValue::makeInterruptible())
{
    m_handler.onActivated([this](double value)
    {
        if (!qFuzzyCompare(m_value, value))
        {
            m_value = value;
            emit valueChanged();
        }

        setActive(true);

    }).onDeactivated([this]()
    {
        setActive(false);
    });
}

void Action_B_RequestPresenter::setActive(bool value)
{
    if (m_active != value)
    {
        m_active = value;
        emit activeChanged();
    }
}

void Action_B_RequestPresenter::setValue(double value)
{
    m_handler.reply(value);
}
