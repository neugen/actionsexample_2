#include "Action_C_RequestPresenter.h"

Action_C_RequestPresenter::Action_C_RequestPresenter(QObject* parent)
    : QObject(parent)
    , m_handler(RetrieveSomeAction::makeInterruptible())
{
    m_handler.onActivated([this]()
    {
        setActive(true);

    }).onDeactivated([this]()
    {
        setActive(false);
    });
}

void Action_C_RequestPresenter::setActive(bool value)
{
    if (m_active != value)
    {
        m_active = value;
        emit activeChanged();
    }
}

void Action_C_RequestPresenter::justDoIt()
{
    m_handler.reply({});
}
